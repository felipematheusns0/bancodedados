package br.ifsc.edu.bd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = openOrCreateDatabase("meubd", MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS notas("+
                "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "titulo VARCHAR NOT NULL,"+
                "texto VARCHAR);");

        ContentValues mContent = new ContentValues();
        mContent.put("titulo", "Agora estou mais feliz");
        mContent.put("texto","Hoje eu acordei feliz porque ai programar");

        db.insert("notas",null,mContent );
        Cursor cursor = db.rawQuery("SELECT * FROM notas", null, null);
        cursor.moveToFirst();

        String id, titulo, texto;

        ArrayList<String> arrayList = new ArrayList<>();

        while (!cursor.isAfterLast()) {
            //id = cursor.getString(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            //texto = cursor.getString(cursor.getColumnIndex("texto"));
            arrayList.add(titulo);

            //Log.d("TabelaNotas", id + titulo + texto);

            cursor.moveToNext();
            }

        listView = findViewById(R.id.listView);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                arrayList);

        listView.setAdapter(adapter);


    }
}
